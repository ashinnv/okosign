package camOp

import (
	"fmt"
	"sync"
	"time"

	"gocv.io/x/gocv"
)

func RunCam(targDev int, matChan chan gocv.Mat) {

	var mtx sync.Mutex
	webcam, err := gocv.OpenVideoCapture(targDev)
	if err != nil {
		fmt.Printf("Error opening video capture device: %v\n", 0)
		return
	}
	defer webcam.Close()

	img := gocv.NewMat()
	defer img.Close()

	for {
		//Tempoary rate limiting
		time.Sleep(1 * time.Second)
		go func() {

			mtx.Lock()
			if ok := webcam.Read(&img); !ok {
				fmt.Printf("Device closed: %v\n", 0)
				return
			}
			mtx.Unlock()

			if img.Empty() {
				fmt.Println("Bad frame.")
			}

			matChan <- img
		}()
	}
}
