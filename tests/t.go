package main

import (
	"fmt"
	"time"

	"gitlab.com/ashinnv/oddstring"
)

func main() {

	strChan := make(chan string)

	go oddstring.RandStringChanSimple(128, strChan, 4)

	for tmp := range strChan {
		fmt.Println(tmp)
		time.Sleep(1 * time.Second)
	}
}
