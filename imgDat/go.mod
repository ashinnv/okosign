module imgDat

go 1.20

require (
	github.com/otiai10/gosseract/v2 v2.4.0
	gitlab.com/ashinnv/oddstring v0.1.1
	gocv.io/x/gocv v0.33.0
)

replace gitlab.com/ashinnv/oddstring => ../oddstring
