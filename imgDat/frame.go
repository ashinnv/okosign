package imgDat

import (
	"bytes"
	"fmt"
	"image"
	"time"

	"github.com/otiai10/gosseract/v2"
	"gitlab.com/ashinnv/oddstring"
	"gocv.io/x/gocv"
)

/*
structs and methods for storing and manipulating image files
*/
type Frame struct {
	Name      string
	Width     uint64
	Height    uint64
	ImgDat    []byte
	Filetype  string
	ContText  string
	Timestamp uint64
	GoType    gocv.MatType
	Channels  int
}

/*MatToFrameFactory
** Consume mats read from camera and transform them into
** frames that get sent off down the pipeline. This function
** should prioritize reducing latency.
** TODO: Maybe we fold this function into the CamRead function
** to prevent latency issues.
 */
func MatToFrameFactory(input chan gocv.Mat, output chan Frame) {
	//var mtx sync.Mutex
	for tmp := range input {

		go func(npt gocv.Mat) {

			output <- Frame{
				oddstring.RandStringSingle(128),
				uint64(npt.Cols()),
				uint64(npt.Rows()),
				npt.ToBytes(),
				"",
				"",
				uint64(time.Now().UnixNano()),
				npt.Type(),
				npt.Channels(),
			}

		}(tmp)
	}
}

/*
	strChan := make(chan string)

	go oddstring.RandStringChanSimple(128, strChan, 4)

	for tmp := range strChan {
		fmt.Println(tmp)
		time.Sleep(1 * time.Second)
	}
*/
//TODO: handle errors properly instead of just returning nil
func (frm *Frame) ReadText() error {
	client := gosseract.NewClient()
	defer client.Close()

	encImg, encErr := gocv.NewMatFromBytes(int(frm.Height), int(frm.Width), frm.GoType, frm.ImgDat)
	if encErr != nil {
		fmt.Println("Error encoding in ReadText:", encErr)
		return encErr
	}

	//fmt.Printf("-------------\nChannels: %d\nWidth: %d\nHeight: %d\n-----------------\n", encImg.Channels(), encImg.Cols(), encImg.Rows())

	jpgDat, encErr2 := gocv.IMEncode(".png", encImg)
	if encErr2 != nil {
		fmt.Println("Error encoding in ReadText (part 2):", encErr2)
		return encErr2
	}

	//fmt.Printf("------------>>\nDone with IMEncode\n------------->>\n")
	client.SetImageFromBytes(jpgDat.GetBytes())
	var textError error
	frm.ContText, textError = client.Text()
	if textError != nil {
		fmt.Println("Text error in reading image: ", textError)
		return textError
	}

	return nil
}

// Create a jpg thumbnail of given dimensions
func (frm Frame) GenerateThumbnail(width, height int) image.Image {

	mat, err := gocv.NewMatFromBytes(int(frm.Height), int(frm.Width), frm.GoType, frm.ImgDat)
	if err != nil {
		panic("Failed to convert frame to mat: " + err.Error())
	}

	resizePoint := image.Point{width, height}

	//func Resize(src Mat, dst *Mat, sz image.Point, fx, fy float64, interp InterpolationFlags)
	gocv.Resize(mat, &mat, resizePoint, 0, 0, 0)

	//Make the mat into a byteslice of a png
	encDat, encErr := gocv.IMEncode("png", mat)
	if encErr != nil {
		panic("Failed to convert mat to pngDat: " + encErr.Error())
	}

	encBytes := encDat.GetBytes()

	// Create a new reader from the data
	reader := bytes.NewReader(encBytes)

	// Decode the data into an image.Image
	img, _, err := image.Decode(reader)
	if err != nil {
		panic("Failed to decode bytes to image.Image: " + err.Error())
	}

	return img

}
