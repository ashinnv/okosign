package imgDat

import (
	"bytes"
	"database/sql"
	"encoding/gob"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/ashinnv/oddstring"
	"gocv.io/x/gocv"
)

func runConverter(input chan gocv.Mat, output chan Frame) {
	randString := oddstring.RandStringSingle(45)
	for tmpMat := range input {
		output <- Frame{
			randString,
			uint64(tmpMat.Cols()),
			uint64(tmpMat.Rows()),
			tmpMat.ToBytes(),
			"",
			"",
			uint64(time.Now().UnixNano()),
			tmpMat.Type(),
			tmpMat.Channels(),
		}
	}
}

func ReadSign(input chan Frame, output chan Frame) {
	for tFrm := range input {
		err := tFrm.ReadText()
		if err != nil {
			fmt.Println("Couldn't read tFrame:", err)
			panic("Couldn't read tFrame: " + err.Error())
		}

		output <- tFrm
	}
}

func WriteData(input chan Frame) {

	db, err := sql.Open("mysql", "root:313231a@tcp(localhost:3306)/capperCloud")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Name, blob, text, thumbnail
	stat, err := db.Prepare("INSERT INTO sign_reads VALUES (?, ?, ?, ?)")
	if err != nil {
		panic(err.Error())
	}

	var buff bytes.Buffer

	for frm := range input {
		enc := gob.NewEncoder(&buff)
		blobErr := enc.Encode(frm)
		if blobErr != nil {
			fmt.Println("Blob error:", blobErr)
		}

		_, execErr := stat.Exec(frm.Name, buff, frm.ContText, frm.GenerateThumbnail(640, 480))
		if execErr != nil {
			fmt.Println("Exec error:", execErr)
		}

	}
}
