module signReader

go 1.20

require (
	camOp v1.1.1
	gocv.io/x/gocv v0.33.0
	imgDat v1.1.1
)

replace camOp => ../camOp

replace imgDat => ../imgDat
