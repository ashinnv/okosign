package main

import (
	"camOp"
	"flag"
	"fmt"

	"imgDat"

	"gocv.io/x/gocv"
)

func main() {

	targCam := flag.Int("targCam", 0, "Target device that we'll be pulling the camera data from.")
	flag.Parse()

	camChan := make(chan gocv.Mat)
	conChan := make(chan imgDat.Frame)
	obChan := make(chan imgDat.Frame)

	go camOp.RunCam(*targCam, camChan)
	go imgDat.MatToFrameFactory(camChan, conChan)
	go imgDat.ReadSign(conChan, obChan)
	//go imgDat.WriteData(obChan)
	go writeData(obChan)
	stopper := make(chan bool)
	_ = <-stopper
}

func writeData(input chan imgDat.Frame) {
	fmt.Println("Starting writeData")
	for tmp := range input {
		fmt.Println(tmp.ContText)
	}
}
